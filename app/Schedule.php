<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    //
    protected $table = "schedules";

    public function from()
    {
      return $this->belongsTo('App\Place', 'from_place_id');
    }

    public function to()
    {
      return $this->belongsTo('App\Place', 'to_place_id');
    }
}
