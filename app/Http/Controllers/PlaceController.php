<?php

namespace App\Http\Controllers;

use App\Place;
use Illuminate\Http\Request;

class PlaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $place = Place::all();
        return view('admin.place.place', ['places' => $place]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.place.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $place = new Place;
        $place->name = $request->name;
        $place->latitude = $request->latitude;
        $place->longitude = $request->longitude;
        $place->x = $request->x;
        $place->y = $request->y;

        $image = $request->image->store('public/place');

        $place->image_path = basename($image);

        $place->description = $request->description;

        $place->save();

        return redirect('/admin/place');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $place = Place::find($id);
        $place->delete();

        return back();
    }

    /**
     * Mencari place, dengan autocomplete
     *
     * @param  string  $name
     * @return \Illuminate\Http\Response
     */
    public function searchPlace($name)
    {
        //kalau jumlah karakter $name lebih dari 3 maka dia nyari place
        if (strlen($name) > 3) {
          $place = Place::where('name', $name)->orWhere('name', 'like', '%' . $name . '%')->get();
          return response()->json([
            'message' => 'OKE!',
            'status' => 200,
            'data' => $place
          ]);
        }

        return response()->json([
          'message' => 'OKE!',
          'status' => 200,
          'data' => []
        ]);
    }
}
