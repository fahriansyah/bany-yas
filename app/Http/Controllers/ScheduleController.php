<?php

namespace App\Http\Controllers;

use App\Place;
use App\Schedule;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $schedule = Schedule::all();
        return view('admin.schedule.schedule', ['schedules' => $schedule]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $place = Place::all();
        return view('admin.schedule.create', ['places' => $place]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $schedule = new Schedule;
        $schedule->type = $request->type;
        $schedule->from_place_id = $request->from_place_id;
        $schedule->to_place_id = $request->to_place_id;
        $schedule->departure_time = $request->departure_time;
        $schedule->arrival_time = $request->arrival_time;
        $schedule->speed = $request->speed;
        $schedule->distance = $request->distance;
        $schedule->line = $request->line;
        $schedule->save();

        return redirect('/admin/schedule');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Cari rute jalan.
     *
     * @param  int  $from_place_id, int $to_place_id, datetime $departure_time
     * @return \Illuminate\Http\Response
     */
    public function searchRoute($from_place_id, $to_place_id, $departure_time)
    {
      $schedule = Schedule::with(['from', 'to'])->where('from_place_id', $from_place_id)->where('to_place_id', $to_place_id)->where('departure_time', '=', $departure_time)->get();

      if (!$schedule) {
        return response()->json([
          'message' => 'Not found',
          'status' => 404,
          'data' => []
        ]);
      }

      return response()->json([
        'message' => 'OKE!',
        'status' => 200,
        'data' => $schedule
      ]);
    }
}
