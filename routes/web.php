<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//route admin
Route::get('/admin/login', 'AdminLoginController@showLoginForm');
Route::post('/admin/login/submit', 'AdminLoginController@login')->name('admin.login');
Route::get('/admin/logout', 'AdminLoginController@logout')->name('admin.logout');

Route::get('/admin/home' , 'AdminController@home')->middleware('auth:admin');

Route::get('/admin/place/', 'PlaceController@index')->middleware('auth:admin');
Route::get('/admin/place/create', 'PlaceController@create')->name('place.create')->middleware('auth:admin');
Route::post('/admin/place/store', 'PlaceController@store')->name('place.store')->middleware('auth:admin');
Route::get('/admin/place/delete/{id}', 'PlaceController@destroy')->name('place.destroy')->middleware('auth:admin');

Route::get('/admin/schedule/', 'ScheduleController@index')->middleware('auth:admin');
Route::get('/admin/schedule/create', 'ScheduleController@create')->name('schedule.create')->middleware('auth:admin');
Route::post('/admin/schedule/store', 'ScheduleController@store')->name('schedule.store')->middleware('auth:admin');


//route user
Route::get('/search/place/{name}/{token}', 'PlaceController@searchPlace')->middleware('api.user');
Route::get('/search/route/{from_place_id}/{to_place_id}/{departure_time}/{token}', 'ScheduleController@searchRoute')->middleware('api.user');
