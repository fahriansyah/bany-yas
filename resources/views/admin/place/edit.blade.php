@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-10">
      <div class="card">
        <div class="card-header">Input Place</div>

        <div class="card-body">
          <form action="index.html" method="post">
            <div class="form-row">
              <div class="form-group col">
                <label>Name</label>
                <input type="text" name="" value="" class="form-control">
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col">
                <label>Latitude</label>
                <input type="text" name="" value="" class="form-control">
              </div>
              <div class="form-group col">
                <label>Longitude</label>
                <input type="text" name="" value="" class="form-control">
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col">
                <label>X</label>
                <input type="text" name="" value="" class="form-control">
              </div>
              <div class="form-group col">
                <label>Y</label>
                <input type="text" name="" value="" class="form-control">
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col">
                <label>Image</label>
                <input type="file" name="" value="" class="form-control">
              </div>
            </div>

            <button type="submit" name="button" class="btn btn-primary">Simpan</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
