@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">Place</div>

        <div class="card-body">
          <a href="{{ route('place.create') }}" class="btn btn-outline-info mb-2">Tambah</a>
          <table class="table table-stripped">
            <thead>
              <tr>
                <th>No</th>
                <th>Name</th>
                <th>Latitude</th>
                <th>Longitude</th>
                <th>x</th>
                <th>y</th>
                <th>photo</th>
                <th>action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($places as $key => $place)
              <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $place->name }}</td>
                <td>{{ $place->latitude }}</td>
                <td>{{ $place->longitude }}</td>
                <td>{{ $place->x }}</td>
                <td>{{ $place->y }}</td>
                <td><img src="/storage/place/{{ $place->image_path }}" style="width:250px"></td>
                <td>
                  <a href="#" class="btn btn-outline-primary">Edit</a>
                  <a href="{{ route('place.destroy', $place->id) }}" class="btn btn-outline-primary">Delete</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
