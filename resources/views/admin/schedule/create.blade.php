@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-10">
      <div class="card">
        <div class="card-header">Input Schedule</div>

        <div class="card-body">
          <form action="{{ route('schedule.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-row">
              <div class="form-group col">
                <label>Type</label>
                <select class="form-control" name="type">
                  <option value="Bus">Bus</option>
                  <option value="Train">Train</option>
                </select>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col">
                <label>From Place</label>
                <select class="form-control" name="from_place_id">
                  @foreach($places as $place)
                    <option value="{{ $place->id }}">{{ $place->name }}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group col">
                <label>To Place</label>
                <select class="form-control" name="to_place_id">
                  @foreach($places as $place)
                    <option value="{{ $place->id }}">{{ $place->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col">
                <label>Departure Time</label>
                <input type="datetime-local" name="departure_time" value="" class="form-control">
              </div>
              <div class="form-group col">
                <label>Arrival Time</label>
                <input type="datetime-local" name="arrival_time" value="" class="form-control">
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col">
                <label>Speed</label>
                <input type="text" name="speed" class="form-control">
              </div>
              <div class="form-group col">
                <label>Distance</label>
                <input type="text" name="distance" class="form-control">
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col">
                <label>Line</label>
                <input type="text" name="line" class="form-control">
              </div>
            </div>

            <button type="submit" name="button" class="btn btn-primary">Simpan</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
