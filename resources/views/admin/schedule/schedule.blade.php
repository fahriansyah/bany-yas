@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">Schedule</div>

        <div class="card-body">
          <a href="{{ route('schedule.create') }}" class="btn btn-outline-info mb-2">Tambah</a>
          <table class="table table-stripped">
            <thead>
              <tr>
                <th>No</th>
                <th>From place</th>
                <th>To place</th>
                <th>Departure Time</th>
                <th>Arrival Time</th>
                <th>Distance</th>
                <th>Speed</th>
                <th>action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($schedules as $key => $schedule)
              <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $schedule->from->name }}</td>
                <td>{{ $schedule->to->name }}</td>
                <td>{{ Carbon\Carbon::parse($schedule->departure_time)->toDayDateTimeString() }}</td>
                <td>{{ Carbon\Carbon::parse($schedule->arrival_time)->toDayDateTimeString() }}</td>
                <td>{{ $schedule->distance }}</td>
                <td>{{ $schedule->speed }}</td>
                <td>
                  <a href="#" class="btn btn-outline-primary">Edit</a>
                  <a href="{{ route('place.destroy', $schedule->id) }}" class="btn btn-outline-primary">Delete</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
