@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-4">
      <div class="card">
        <div class="card-header">Bany Yas</div>

        <div class="card-body">
          <form action="index.html" method="post">
            <div class="form-group">
              <label>From Place</label>
              <input type="text" name="" id="edtFromPlace" class="form-control">
            </div>
          </form>

          <div class="list-group" id="listResultFrom">

          </div>

          <form class="mt-3" action="index.html" method="post">
            <div class="form-group">
              <label>To Place</label>
              <input type="text" name="" id="edtToPlace" class="form-control">
            </div>
          </form>

          <div class="list-group" id="listResultTo">

          </div>

          <div class="form-input">
            <label>Departure time</label>
            <input type="datetime-local" name="" id="departureTime" class="form-control">
          </div>

          <button id="btnSearchRoute" type="button" name="button" class="btn btn-outline-primary mt-3">Search Route</button>

        </div>
      </div>
    </div>

    <div class="col-md-8">
      <div class="card">
        <div class="card-body">
          <img id="mapPreview" src="" alt="" class="img-fluid">
        </div>
      </div>
    </div>

    <div class="col-md-12 mt-4">
      <div class="card">
        <div class="card-body">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>From place</th>
                <th>To place</th>
                <th>Departure time</th>
                <th>Arival time</th>
                <th>Distance</th>
                <th>Speed</th>
                <th>Line</th>
              </tr>
            </thead>
            <tbody>
              <tr id="listResultRoute">

              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">

  // ini set variable buat nampung id from_place, dan to_place
  // nanti variable ini buat nyari route
  let from_id = 0;
  let to_id = 0;


  //saat user mengetik di form place dia bakal jalanin fungsi searchFormPlace();
  $(document).on('keyup', '#edtFromPlace', function(){
    searchFromPlace($('#edtFromPlace').val());
  });

  //fungsi searchFromPlace() buat me request ke API /search/place buat nyari PLACE from berdasarkan nama
  function searchFromPlace(name) {
    $.ajax({
      url:'/search/place/' + name + '/D7mnCHtfWQezTKbFAcXWi04qMbUuXbvUPqOz',
      type:'GET',
      success:function(data){
        $('#listResultFrom').html('')
        $.each(data.data, function(key, value){
          $('#listResultFrom').append('<button id="btnDetailFrom" data-id="' + value.id + '" data-img="' + value.image_path + '" data-name="' + value.name + '" type="button" class="list-group-item list-group-item-action">' + value.name + '</button>');
        })
      }
    })
  }

  //saat user mengetik di form place dia bakal jalanin fungsi searchFormPlace();
  $(document).on('keyup', '#edtToPlace', function(){
    searchToPlace($('#edtToPlace').val())
  });

  //fungsi searchFromPlace() buat me request ke API /search/place buat nyari PLACE to berdasarkan nama
  function searchToPlace(name) {
    $.ajax({
      url:'/search/place/' + name + '/D7mnCHtfWQezTKbFAcXWi04qMbUuXbvUPqOz',
      type:'GET',
      success:function(data){
        $('#listResultTo').html('')
        // ini foreach
        $.each(data.data, function(key, value){
          $('#listResultTo').append('<button id="btnDetailTo" data-id="' + value.id + '" data-img="' + value.image_path + '" data-name="' + value.name + '" type="button" class="list-group-item list-group-item-action">' + value.name + '</button>');
        })
      }
    })
  }

  //saat user klik hasil pencarian place from
  $(document).on('click', '#btnDetailFrom', function(){
    //me set gambar sesuai hasil place
    $('#mapPreview').attr('src', '/storage/place/' + $(this).data("img"));
    //me set value edittext from place
    $('#edtFromPlace').val($(this).data('name'))
    searchFromPlace($(this).data('name'))
    //me set isi variable from_id sesuai id place yg pilihan user
    from_id = $(this).data('id');
  });

  //saat user klik hasil pencarian place to
  $(document).on('click', '#btnDetailTo', function(){
    //me set gambar sesuai hasil place
    $('#mapPreview').attr('src', '/storage/place/' + $(this).data("img"));
    //me set value edittext from place
    $('#edtToPlace').val($(this).data('name'))
    searchToPlace($(this).data('name'))
    //me set isi variable from_id sesuai id place yg pilihan user
    to_id = $(this).data('id');
  });


  //disini ketika user menclick tombol Search Route
  $(document).on('click', '#btnSearchRoute', function(){
    //mengambil value departure time
    let departureTime = $('#departureTime').val()
    //me request ke api untuk mencari route sesuai from_place_id, to_place_id, dan departure time
    $.ajax({
      url: '/search/route/' + from_id + '/' + to_id + '/' + departureTime + '/D7mnCHtfWQezTKbFAcXWi04qMbUuXbvUPqOz',
      type: 'GET',
      success:function(data){
        $('#listResultRoute').html('')
        //ini foreach
        $.each(data.data, function(key, value){
          //membuat table
          $('#listResultRoute').append(
            "<td>" + value.from.name + "</td>" +
            "<td>" + value.to.name + "</td>" +
            "<td>" + value.departure_time + "</td>" +
            "<td>" + value.arrival_time + "</td>" +
            "<td>" + value.distance + "</td>" +
            "<td>" + value.speed + "</td>" +
            "<td>" + value.line + "</td>"
          )
        })
      }
    })
  })

</script>
@endsection
